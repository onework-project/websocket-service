package main

import (
	"log"

	_ "github.com/lib/pq"

	"gitlab.com/onework-project/websocket-service/config"
	"gitlab.com/onework-project/websocket-service/websocket"

	grpcPkg "gitlab.com/onework-project/websocket-service/pkg/grpc_client"
	"gitlab.com/onework-project/websocket-service/pkg/logger"
)

func main() {
	cfg := config.Load(".")
	logger.Init()
	logger := logger.GetLogger()
	grpcConn, err := grpcPkg.New(cfg)
	if err != nil {
		log.Fatalf("failed to get grpc connections: %v", err)
	}

	websocket.Run(cfg, grpcConn, &logger)
}

package grpc_client

import (
	"fmt"

	"gitlab.com/onework-project/websocket-service/config"
	pba "gitlab.com/onework-project/websocket-service/genproto/auth_service"
	pbc "gitlab.com/onework-project/websocket-service/genproto/chat_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type GrpcClientI interface {
	MessageService() pbc.ChatMessageServiceClient
	ChatService() pbc.ChatServiceClient
	AuthService() pba.AuthServiceClient
}

type GrpcClient struct {
	cfg         config.Config
	connections map[string]interface{}
}

func New(cfg config.Config) (GrpcClientI, error) {
	connChatMessageService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ChatServiceHost, cfg.ChatServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("chat service dial host: %s port:%s err: %v",
			cfg.ChatServiceHost, cfg.ChatServiceGrpcPort, err)
	}
	connChatService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ChatServiceHost, cfg.ChatServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("chat service dial host: %s port:%s err: %v",
			cfg.ChatServiceHost, cfg.ChatServiceGrpcPort, err)
	}
	connAuthService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.AuthServiceHost, cfg.AuthServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("auth service dial host: %s port:%s err: %v",
			cfg.ChatServiceHost, cfg.ChatServiceGrpcPort, err)
	}

	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"message_service": pbc.NewChatMessageServiceClient(connChatMessageService),
			"chat_service":    pbc.NewChatServiceClient(connChatService),
			"auth_service":    pba.NewAuthServiceClient(connAuthService),
		},
	}, nil
}

func (g *GrpcClient) MessageService() pbc.ChatMessageServiceClient {
	return g.connections["message_service"].(pbc.ChatMessageServiceClient)
}

func (g *GrpcClient) ChatService() pbc.ChatServiceClient {
	return g.connections["chat_service"].(pbc.ChatServiceClient)
}

func (g *GrpcClient) AuthService() pba.AuthServiceClient {
	return g.connections["auth_service"].(pba.AuthServiceClient)
}

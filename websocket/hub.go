package websocket

import (
	"context"
	"encoding/json"

	"gitlab.com/onework-project/websocket-service/genproto/chat_service"
	grpcPkg "gitlab.com/onework-project/websocket-service/pkg/grpc_client"
	"gitlab.com/onework-project/websocket-service/pkg/logger"
)

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients.
	clients map[int64]*Client

	// Inbound messages from the clients.
	broadcast chan *Message

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client
	grpcClient grpcPkg.GrpcClientI
	logger     *logger.Logger
}

func newHub(grpcClient grpcPkg.GrpcClientI, logger *logger.Logger) *Hub {
	return &Hub{
		broadcast:  make(chan *Message),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[int64]*Client),
		grpcClient: grpcClient,
		logger:     logger,
	}
}

type Message struct {
	UserID  int64  `json:"user_id"`
	ChatID  int64  `json:"chat_id"`
	Message string `json:"message"`
}

func (h *Hub) run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client.userID] = client
			h.logger.Infof("Client connected: %d", client.userID)
		case client := <-h.unregister:
			if _, ok := h.clients[client.userID]; ok {
				delete(h.clients, client.userID)
				close(client.send)
			}
			h.logger.Infof("Client disconnected: %d", client.userID)
		case data := <-h.broadcast:
			// Create message
			_, err := h.grpcClient.MessageService().Create(context.Background(), &chat_service.CreateChatMessageReq{
				Message: data.Message,
				ChatId:  data.ChatID,
				UserId:  data.UserID,
			})
			if err != nil {
				h.logger.WithError(err).Error("failed to create message")
				continue
			}

			// Get Chat member
			chat, err := h.grpcClient.ChatService().Get(context.Background(), &chat_service.GetChatIdReq{
				ChatId: data.ChatID,
			})
			if err != nil {
				h.logger.WithError(err).Error("failed to get chat")
				continue
			}

			// * if id is company_id, then send message to applicant
			if data.UserID == chat.MemberId {
				client, ok := h.clients[chat.ApplicantId]
				message, err := json.Marshal(data)
				if err != nil {
					close(client.send)
					delete(h.clients, client.userID)
				}
				if ok {
					select {
					case client.send <- message:
					default:
						close(client.send)
						delete(h.clients, client.userID)
					}
				}
			} else { // *  if id is applicant, then send message to company
				client, ok := h.clients[chat.MemberId]
				message, err := json.Marshal(data)
				if err != nil {
					close(client.send)
					delete(h.clients, client.userID)
				}
				if ok {
					select {
					case client.send <- message:
					default:
						close(client.send)
						delete(h.clients, client.userID)
					}
				}
			}
		}
	}
}

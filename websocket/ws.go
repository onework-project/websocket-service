package websocket

import (
	"log"
	"net/http"

	"gitlab.com/onework-project/websocket-service/config"
	grpcPkg "gitlab.com/onework-project/websocket-service/pkg/grpc_client"
	"gitlab.com/onework-project/websocket-service/pkg/logger"
)

func Run(cfg config.Config, grpcClient grpcPkg.GrpcClientI, logger *logger.Logger) {
	hub := newHub(grpcClient, logger)
	go hub.run()

	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		serveWs(hub, w, r, grpcClient)
	})

	log.Println("Websocket server started in port ", cfg.WsPort)
	err := http.ListenAndServe(cfg.WsPort, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
